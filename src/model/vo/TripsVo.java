package model.vo;

public class TripsVo implements Comparable<TripsVo>{
	private int RouteId;
	private int TripId;
	private int Block_id;
	
	//Getters y setters
	public int getRouteId() {
		return RouteId;
	}
	public TripsVo(int routeId, int tripId, int block_id) {
	
		RouteId = routeId;
		TripId = tripId;
		Block_id = block_id;
	}
	public void setRouteId(int routeId) {
		RouteId = routeId;
	}
	public int getBlock_id() {
		return Block_id;
	}
	public void setBlock_id(int block_id) {
		Block_id = block_id;
	}
	public int getTripId() {
		return TripId;
	}
	public void setTripId(int tripId) {
		TripId = tripId;
	}
	/**
	 *criterio de comparacion por blockid. 
	 */
	public int compareTo(TripsVo arg0) {
		if(Block_id-(arg0.Block_id)==0)
		return TripId-arg0.TripId   ;
		else
			return Block_id-(arg0.Block_id);
	}


}
