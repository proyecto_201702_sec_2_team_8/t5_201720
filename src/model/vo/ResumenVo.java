package model.vo;

import model.data_structures.SimpleArrayLIst;

public class ResumenVo implements Comparable<ResumenVo> 
{
	private int Block_id;
	private int Route_id;
	private int Total_block_id;
	private SimpleArrayLIst<Integer> arregloTripId;
	private int Prioridad;

	public int getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(int prioridad) {
		Prioridad = prioridad;
	}
	public int getBlock_id() {
		return Block_id;
	}
	public void setBlock_id(int block_id) {
		Block_id = block_id;
	}
	public int getRoute_id() {
		return Route_id;
	}
	public void setRoute_id(int route_id) {
		Route_id = route_id;
	}
	public int getTotal_block_id() {
		return Total_block_id;
	}
	public void setTotal_block_id(int total_block_id) {
		Total_block_id = total_block_id;
	}
	public SimpleArrayLIst<Integer> getArregloTripId() {
		return arregloTripId;
	}
	public void setArregloTripId(SimpleArrayLIst<Integer> temporal) {
		this.arregloTripId = temporal;
		int[] arreglo = new int[temporal.size()];
		for (int i = 0; i < temporal.size(); i++) {
			arreglo[i]=temporal.get(i);
		}
		HeapSort(arreglo, arreglo.length);
	}
	@Override
	public int compareTo(ResumenVo o) {
		
			return Route_id-o.Route_id;
		

	}
	public void minHeap(int arr[],int tamanio )

	{
		//	Variables para cotrolar el heap
		int i,left,right,temp;
		//Vraible para contorlar el swap
		int val;
		
		for(i=tamanio/2; i>=0;i--)
		{
			temp=i;
			left=(2*i)+1;
			right=(2*i)+2;
			if(left < tamanio && arr[left] < arr[temp]) {
				temp = left;
			}

			if(right < tamanio && arr[right] < arr[temp]) {
				temp = right;
			}
			if(temp != i) {
				val = arr[i];
				arr[i] = arr[temp];
				arr[temp] = val;
				minHeap(arr, tamanio);
			}
			
		}
	}
	public  void HeapSort(int arr[],int tamanio )
	{
		int i, j;
		int [] sorted= new int[tamanio] ; 

		for(i = tamanio , j = 0; i >= 0 && j < tamanio; i--, j++) {
			minHeap(arr, i);
			sorted[j] = arr[0];
			arr[0] = arr[i-1];	//put last element to root node
		}

		for(i = 0; i <tamanio; i++) 
		{
			arr[i] = sorted[i];
		}

	}



}
