package model.vo;

public class RutasVO implements Comparable<RutasVO>
{
	private int routeId;
	private int Contador;

	public RutasVO(int route_id, int contador) {
		routeId=route_id;
		Contador=contador;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getContador() {
		return Contador;
	}

	public void setContador(int contador) {
		Contador = contador;
	}

	@Override
	public int compareTo(RutasVO o) {
		// TODO Auto-generated method stub
		return o.Contador-Contador;
	}

}
