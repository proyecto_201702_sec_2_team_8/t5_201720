package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList.Node;

public class DoubleLinkedList<T> implements IList<T>
{

	private Node<T> primero;
	private Node<T> ultimo;
	private Iterator<T> iterador;
	private Integer tamanio;
	public DoubleLinkedList ()
	{
		tamanio=0;
		primero=null;
		ultimo=primero;

	}
	public static class Node<T>
	{
		T Item ;
		Node<T> siguiente;
		Node<T> anterior;
		public Node()
		{
			Item=null;
			siguiente=null;
			anterior=null;
		}	
		public void cambiarElemento(T item)
		{
			Item=item;
		}
		public Node<T> darAnterior()
		{
			return anterior;
		}
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		public T darElemento()
		{
			return Item;
		}
		public void cambiarAnterior(Node<T> nuevo)
		{
			anterior=nuevo;
		}
		public void cambiarSiguiente(Node<T> nuevo)
		{
			siguiente=nuevo;
		}
	}
	public class iterador<T> implements Iterator<T>
	{
		private Node actual;
		public iterador() 
		{
			actual= primero;
		}

		public boolean hasNext()
		{
			return actual!=null;

		}


		public T next()
		{
			Node<T> ahora=actual;
			T item = ahora.darElemento();
			actual=actual.darSiguiente();
			return item;
		}

		public Node<T> darActual()
		{
			return actual;
		}


		public void remove()
		{
			throw new UnsupportedOperationException();	

		}


	};



	public Iterator<T> iterator()
	{
		return new iterador<T>();
	}

	public Integer getSize() 
	{
		return tamanio;
	}


	public void add(T item)
	{
		Node<T> nuevo=new Node<T>(); 
		nuevo.cambiarElemento(item);
		if(primero==null)
		{
			primero=nuevo;
			ultimo=nuevo;
			iterador=iterator();
		}
		else
		{
			nuevo.cambiarSiguiente(primero);
			primero.cambiarAnterior(nuevo);
			primero=nuevo;
			iterador=iterator();
		}	
		tamanio++;
	}


	public void addAtEnd(T item)
	{
		Node<T> nuevo=new Node<T>(); 
		nuevo.cambiarElemento(item);
		if(ultimo==null)
		{
			ultimo=nuevo;
			primero=nuevo;
			iterador=iterator();
		}
		else
		{
			nuevo.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(nuevo);
			ultimo=nuevo;
		}	
		tamanio++;
	}


	public void addAtK(Integer indice, T item)
	{
		if(indice==0)
		{
			add(item);
		}
		else if(indice==(getSize()-1))
		{
			addAtEnd(item);
		}
		else
		{
			Integer contador=0;
			Node<T> nuevo=new Node<T>(); 
			nuevo.cambiarElemento(item);
			while(contador<indice-1 && iterador.hasNext())
			{
				contador++;
				iterador.next();
			}	
			Node<T> actual=((iterador<T>) iterador).darActual();
			nuevo.cambiarSiguiente(actual.darSiguiente());
			nuevo.cambiarAnterior(actual);
			nuevo.darSiguiente().cambiarAnterior(nuevo);
			nuevo.darAnterior().cambiarSiguiente(nuevo);
		}
		iterador=iterator();
		tamanio++;
	}


	public T getElement(Integer indice) 
	{
		Integer contador=0;
		while(contador<indice && iterador.hasNext())
		{
			contador++;
			iterador.next();
		}		
		T elemento=iterador.next();
		iterador=iterator();
		return elemento;
	}


	public T getCurrentElement() 
	{

		return ((iterador<T>) iterador).darActual().darElemento();
	}

	public void delete(T item) 
	{
		if(primero.darElemento()==item)
		{
			primero.darSiguiente().cambiarAnterior(null);
			primero=primero.darSiguiente();
		}
		else if(ultimo.darElemento()==item)
		{
			ultimo.darAnterior().cambiarSiguiente(null);
			ultimo=ultimo.darAnterior();
		}
		else
		{
			Boolean ya=false;
			while(iterador.hasNext()&& !ya)
			{
				Node<T> actual=((iterador<T>) iterador).darActual();
				if(actual.darSiguiente().darElemento()==item)
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					actual.darSiguiente().cambiarAnterior(actual);
					ya=true;
				}
				iterador.next();
			}
		}
		iterador=iterator();
		tamanio--;
	}


	public void deleteAtK(Integer indice) 
	{
		if(indice==0)
		{
			primero.darSiguiente().cambiarAnterior(null);
			primero=primero.darSiguiente();
		}
		else if(indice==getSize()-1)
		{
			ultimo.darAnterior().cambiarSiguiente(null);
			ultimo=ultimo.darAnterior();
		}
		else
		{
			Integer contador=0;
			while(contador<indice-1 && iterator().hasNext())
			{
				contador++;
				iterador.next();
			}		
			Node<T>actual=((iterador<T>) iterador).darActual();
			actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
			actual.darSiguiente().cambiarAnterior(actual);
		}
		iterador=iterator();
		tamanio--;
	}
	public Node<T> darPrimero()
	{
		return primero;
	}
	public Node<T> darUltimo()
	{
		return ultimo;
	}
	public boolean next()
	{
		return ((iterador<T>) iterador).darActual().darSiguiente()!=null;

	}
	public boolean previous()
	{
		return ((iterador<T>) iterador).darActual().darAnterior()!=null;

	}
	public Iterator<T> darIterador()
	{
		return iterador;
	}


	public void cambiar(IList<T> lista, T ob1, T ob2, int x, int y) 
	{
		ordenadores.cambiar(lista, x, y);

	}

	public DoubleLinkedList ordenar2(IList<T> lista) 
	{
		return ordenadores.mergeSort((DoubleLinkedList)lista);	
	}

	public Node<T> getNode(Integer indice) 
	{
		if(indice==0)
			return primero;
		else if(indice==getSize()-1)
			return ultimo;
		else
		{
			Integer contador=0;
			while(contador<indice && iterador.hasNext())
			{
				contador++;
				iterador.next();
			}		
			Node<T> actual=((iterador<T>) iterador).darActual();
			iterador=iterator();
			return actual;
		}
	}
	public Node<T> darMitad()
	{
		return getNode((tamanio/2)-1);
	}

	public void ordenar(IList lista, int x, int y)
	{
		ordenadores.quickSort(lista, x, y);		
	}
}
