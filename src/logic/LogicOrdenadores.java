package logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.Node;
import model.data_structures.DoubleLinkedList.iterador;
import model.data_structures.IList;
import model.data_structures.MaxPQ;
import model.data_structures.SimpleArrayLIst;
import model.vo.ResumenVo;
import model.vo.RutasVO;
import model.vo.TripsVo;

public class LogicOrdenadores {

	public DoubleLinkedList<TripsVo> ListaDoblementeEnlazada; 
	public DoubleLinkedList<ResumenVo> listaResumenes;
	/**
	 * Inicializa la etructura doblemente enlazada cargando el archivo de trips
	 */
	public void cargarTrips()

	{
		ListaDoblementeEnlazada =new DoubleLinkedList<TripsVo>();
		try {
			BufferedReader lector= new BufferedReader(new FileReader("Data/trips.txt"));
			lector.readLine();
			String linea=lector.readLine();
			while(linea!=null)
			{ 
				String [] arreglo= linea.split(",");
				TripsVo anadido= new TripsVo(Integer.parseInt(arreglo[0]),Integer.parseInt(arreglo[2]),Integer.parseInt(arreglo[6]));
				ListaDoblementeEnlazada.addAtEnd(anadido);

				linea=lector.readLine();
			}
			lector.close();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	/**
	 * Ordena la lista doblemente encadenada de trips por mergesort
	 */
	public void ordenarTrips(){

		ListaDoblementeEnlazada=ListaDoblementeEnlazada.ordenar2(ListaDoblementeEnlazada);

	}
	public void crearListaResumenes()
	{

		listaResumenes=new DoubleLinkedList<>();
		Node<TripsVo> actual=ListaDoblementeEnlazada.darPrimero();
		Node<TripsVo> comparador=ListaDoblementeEnlazada.darPrimero();
		while(actual!=null)
		{
			SimpleArrayLIst<Integer> temporal= new SimpleArrayLIst<Integer>();
			ResumenVo resumen= new ResumenVo();
			resumen.setBlock_id(actual.darElemento().getBlock_id());
			resumen.setRoute_id(actual.darElemento().getRouteId());
			int contador=0;
			while(actual.darElemento().getBlock_id()==comparador.darElemento().getBlock_id())
			{
				temporal.add(actual.darElemento().getTripId());
				contador++;
				if(actual.darSiguiente()!=null)
				{

					if(actual.darSiguiente().darElemento().getBlock_id()==comparador.darElemento().getBlock_id())
						comparador=actual.darSiguiente();
				}
				actual=actual.darSiguiente();
				if(actual==null)
					break;
			}	
			comparador=comparador.darSiguiente();

			resumen.setTotal_block_id(contador);


			resumen.setArregloTripId(temporal);
			if(resumen!=null)
				listaResumenes.add(resumen);

		}
		listaResumenes=listaResumenes.ordenar2(listaResumenes);
		//		for (ResumenVo tripsVo : listaResumenes) {
		//			System.out.println("Block id:"+ tripsVo.getBlock_id() +"  "+ "Route id:"+tripsVo.getRoute_id()+"  "+"Tamanio del arreglo:"+tripsVo.getArregloTripId().size());
		//

	}
	public DoubleLinkedList<RutasVO>obtenerRutas()
	{
		
		DoubleLinkedList<RutasVO> retorno = new DoubleLinkedList<>();

		Iterator<ResumenVo> iter = listaResumenes.iterator();
		
		int a = 0;
		int b = 1;
		
		int contador = 1;
		boolean stop = false;

		while (iter.hasNext() && !stop) {
			
			ResumenVo uno = listaResumenes.getElement(a);
			ResumenVo dos = listaResumenes.getElement(b);

			if (uno.getRoute_id() == dos.getRoute_id()) {
				contador++;
			} 
			else 
			{
				RutasVO ruta = new RutasVO(uno.getRoute_id(), contador);
				retorno.addAtEnd(ruta);
				contador = 1;
			}

			// Avances
			iter.next();
			a++;
			b++;
			
			// Si el avance en b (uno mayor a A), se aumenta en uno el valor de A.
			if (b == listaResumenes.getSize()) {
				a++;
			}

			// Cuando el valor de A es igual al tama�o de Lista, se agrega el �ltimo elemento sin comparaciones.
			if (a == listaResumenes.getSize()) {
				
				RutasVO ruta = new RutasVO(uno.getRoute_id(), contador);
				retorno.addAtEnd(ruta);
				stop = true;
			}
		}
		retorno=retorno.ordenar2(retorno);
		return retorno;
	}
	
	
	public int[] frecuenciaArreglo()
	{
		DoubleLinkedList<RutasVO> routes = obtenerRutas();
		
		Iterator<RutasVO> iter = routes.iterator();
		int[] retorno = new int[routes.getSize()];
		int a = 0;
		
		while(iter.hasNext())
		{
			RutasVO obj = routes.getElement(a);
			retorno[a] = obj.getContador();
			
			// Avance
			iter.next();
			a++;
		}
		
		return retorno;
	}	
	
	
	public IList<RutasVO> listasPQ()
	{
		// Lista de rutas (sin ID repetidos).
		IList<RutasVO> routes = obtenerRutas();
		
		// Lista de retorno
		IList<RutasVO> retorno = new DoubleLinkedList<RutasVO>();
		
		// Uso de la Cola de Prioridad
		int[] frecuencia = frecuenciaArreglo();
		MaxPQ<Integer> pQ = new MaxPQ<Integer>(frecuencia.length);
		
		// Insert
		for(Integer frec: frecuencia)
		{
			pQ.insert(frec);
		}
		
		// Lista auxiliar con las frecuencias ordenadas
		IList<Integer> aux = new DoubleLinkedList<Integer>();
		for(int i= 0; i < frecuencia.length; i++)
		{
			 aux.addAtEnd(pQ.delMax());
		}
		
		// Merge a la lista de Rutas
		routes = routes.ordenar2(routes);
		
		
		
		Iterator<RutasVO> iterUno = routes.iterator();
		int a = 0;
		
		while(iterUno.hasNext())
		{
			RutasVO ruta = routes.getElement(a);
			
			// Iterador lista Auxiliar
			Iterator<Integer> iterDos = aux.iterator();
			int b = 0;
			
			while(iterDos.hasNext())
			{
				Integer intt = aux.getElement(b);
				
				if(ruta.getContador() == intt)
				{
					retorno.addAtEnd(ruta);
				}
				
				// Avance Segunda Lista
				iterDos.next();
				b++;
			}
			// Avances Primera Lista
			iterUno.next();
			a++;
		}
		return retorno;
	}



}
